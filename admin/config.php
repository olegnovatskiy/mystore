<?php
// HTTP
define('HTTP_SERVER', 'http://mystore/admin/');
define('HTTP_CATALOG', 'http://mystore/');

// HTTPS
define('HTTPS_SERVER', 'http://mystore/admin/');
define('HTTPS_CATALOG', 'http://mystore/');

// DIR
define('DIR_APPLICATION', '/home/olegnovatskiy/www/mystore/admin/');
define('DIR_SYSTEM', '/home/olegnovatskiy/www/mystore/system/');
define('DIR_LANGUAGE', '/home/olegnovatskiy/www/mystore/admin/language/');
define('DIR_TEMPLATE', '/home/olegnovatskiy/www/mystore/admin/view/template/');
define('DIR_CONFIG', '/home/olegnovatskiy/www/mystore/system/config/');
define('DIR_IMAGE', '/home/olegnovatskiy/www/mystore/image/');
define('DIR_CACHE', '/home/olegnovatskiy/www/mystore/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/olegnovatskiy/www/mystore/system/storage/download/');
define('DIR_LOGS', '/home/olegnovatskiy/www/mystore/system/storage/logs/');
define('DIR_MODIFICATION', '/home/olegnovatskiy/www/mystore/system/storage/modification/');
define('DIR_UPLOAD', '/home/olegnovatskiy/www/mystore/system/storage/upload/');
define('DIR_CATALOG', '/home/olegnovatskiy/www/mystore/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '12345678');
define('DB_DATABASE', 'mystore');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
