<?php

    class ModelExcelRepotingCreator extends Model
    {
        /**
         * Create a report of products in XLS format.
         *
         * @return bool|string
         */
        public function createProductsReport()
        {
            $this->load->helper('PHPExcel');
            $this->load->helper('PHPExcel/IOFactory');
            $this->load->model('catalog/product');

            $_object_excel = new PHPExcel();
            $_object_excel->setActiveSheetIndex(0);


            $header_row = 2;

            self::createHeaderProductReport($_object_excel, $header_row);
            $current_row = self::outputProductsIntoTableProductReport($_object_excel, $header_row);
            self::writeFooterTextProductReport($_object_excel, $current_row);
            self::setStylesAndOtherSettingsProductReport($_object_excel);

            return self::saveReport($_object_excel);
        }

        /**
         * Methos check a operation system and send path of file to report saving,
         *
         * @param PHPExcel $_object_excel
         */
        private function saveReport(PHPExcel $_object_excel)
        {
            $objWriter = PHPExcel_IOFactory::createWriter($_object_excel, 'Excel5');

            $_user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
            if (substr_count($_user_agent, 'linux') == 0 && substr_count($_user_agent, 'windows') == 0)
            {
                return false;
            }
            elseif(substr_count($_user_agent, 'linux') != 0)
            {
                return self::saveIntoOS($objWriter, '/tmp/catalog_products.xls');
            }
            elseif(substr_count($_user_agent, 'windows') != 0)
            {
                return self::saveIntoOS($objWriter, 'C:\ocstoreReport\catalog_products.xls');
            }

        }

        /**
         * Save report into OS.
         *
         * @param        $objWriter
         * @param string $path
         *
         * @return string
         */
        private function saveIntoOS($objWriter, string $path)
        {
            try{
                if(file_exists($path)){
                    $_save_message = 'Звіт оновлено.';
                }else{
                    $_save_message = 'Звіт сформовано';
                }

                $objWriter->save($path);

                return $_save_message;

            }catch(Exception $e){
                return $e->getMessage();
            }
        }

        /**
         * Create table header of report
         *
         * @param PHPExcel $_object_excel
         * @param int      $header_row
         */
        private function createHeaderProductReport(PHPExcel $_object_excel, int $header_row)
        {
            $_object_excel->getActiveSheet()
                ->setCellValue('A'.$header_row, '№')
                ->setCellValue('B'.$header_row, 'Назва')
                ->setCellValue('C'.$header_row, 'Фото')
                ->setCellValue('D'.$header_row, 'Назва розділу')
                ->setCellValue('E'.$header_row, 'Модель')
                ->setCellValue('F'.$header_row, 'Артикул')
                ->setCellValue('G'.$header_row, 'Ціна')
                ->setCellValue('H'.$header_row, 'Акц. ціна')
                ->setCellValue('I'.$header_row, 'Кількість');

            self::setHeaderStyleCell($_object_excel, 'A', 'I', $header_row);
        }

        /**
         * Output products information into table
         *
         * @param PHPExcel $_object_excel
         * @param int      $header_row
         *
         * @return int      Next row of last product in table
         */
        private function outputProductsIntoTableProductReport(PHPExcel $_object_excel, int $header_row)
        {
            $products = $this->model_catalog_product->getProductsForExcel();
            $this->load->model('tool/image');
           // var_dump($products);

            $current_row = $header_row + 1;

            foreach ($products as $product) {
                $_object_excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $current_row, $current_row - $header_row)
                    ->setCellValue('B' . $current_row, $product['name'])
                    ->setCellValue('D' . $current_row, $product['category_name'])
                    ->setCellValue('E' . $current_row, $product['model'])
                    ->setCellValue('F' . $current_row, $product['sku'])
                    ->setCellValue('G' . $current_row, $product['price'])
                    ->setCellValue('H' . $current_row, $product['action_price'])
                    ->setCellValue('I' . $current_row, $product['quantity']);

                $_object_excel->getActiveSheet()->getRowDimension("{$current_row}")->setRowHeight(65);

                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setName('Sample image');
                $objDrawing->setDescription('Sample image');

                $_image_path = DIR_IMAGE . 'cache/catalog/demo/' . basename($product['image'], '.jpg') . '-80x80.jpg';
                if (!is_file($_image_path)) {
                    if (is_file(DIR_IMAGE . $product['image'])) {
                        $_image_path = DIR_IMAGE . 'cache/catalog/demo/' . basename($this->model_tool_image->resize($product['image'], 80, 80));
                    } else {
                        $_image_path = DIR_IMAGE . 'cache/' . basename($this->model_tool_image->resize('no_image.png', 80, 80));
                    }
                }
                
                $objDrawing->setPath($_image_path);
                //$objDrawing->setHeight(80);
                //$objDrawing->setWidth(40);
                $objDrawing->setCoordinates('C'.$current_row.'');
                $objDrawing->setWorksheet($_object_excel->getActiveSheet());

                $current_row++;
            }

            $row_first_product = $header_row + 1;
            $row_last_product = $current_row - 1;

            $_object_excel->getActiveSheet()->getStyle('A'.$row_first_product.':I'.$row_last_product)->getAlignment()->setVertical(
                PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $border_style_internal_cells= [
                'borders' => [
                    'allborders' => [
                        'style' =>PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => [
                            'argb' => '#000000'
                        ],
                    ],
                ],
            ];

            $border_style_left_cells= [
                'borders' => [
                    'left' => [
                        'style' =>PHPExcel_Style_Border::BORDER_THICK,
                        'color' => [
                            'argb' => '#000000'
                        ],
                    ],
                ],
            ];

            $border_style_right_cells= [
                'borders' => [
                    'right' => [
                        'style' =>PHPExcel_Style_Border::BORDER_THICK,
                        'color' => [
                            'argb' => '#000000'
                        ],
                    ],
                ],
            ];

            $border_style_bottom_cells= [
                'borders' => [
                    'bottom' => [
                        'style' =>PHPExcel_Style_Border::BORDER_THICK,
                        'color' => [
                            'argb' => '#000000'
                        ],
                    ],
                ],
            ];

            $_object_excel->getActiveSheet()->getStyle("A{$row_first_product}:I{$row_last_product}")->applyFromArray($border_style_internal_cells);
            $_object_excel->getActiveSheet()->getStyle("A{$row_first_product}:A{$row_last_product}")->applyFromArray($border_style_left_cells);
            $_object_excel->getActiveSheet()->getStyle("I{$row_first_product}:I{$row_last_product}")->applyFromArray($border_style_right_cells);
            $_object_excel->getActiveSheet()->getStyle("A{$row_last_product}:I{$row_last_product}")->applyFromArray($border_style_bottom_cells);

            return $current_row;
        }

        /**
         * Write text in footer of sheet.
         *
         * @param PHPExcel $_object_excel
         * @param int      $_free_row
         */
        private function writeFooterTextProductReport(PHPExcel $_object_excel, int $_free_row)
        {
            $_free_row++;

            $_object_excel->getActiveSheet()->setCellValue("B{$_free_row}", 'Дата і час створення звіту:');
            $_object_excel->getActiveSheet()->setCellValue("C{$_free_row}", date('Y-m-d, G:i:s', time()));
            $_object_excel->getActiveSheet()->getStyle("B{$_free_row}")->getFont()->setBold(true)->setItalic(true);
            $_object_excel->getActiveSheet()->getStyle("C{$_free_row}")->getFont()->setBold(true);

        }

        /**
         * Set different settings for document and table
         *
         * @param PHPExcel $_object_excel
         */
        private function setStylesAndOtherSettingsProductReport(PHPExcel $_object_excel)
        {
            $this->load->model('user/user');

            $user_info = $this->model_user_user->getUser($this->user->getId());

            if ($user_info) {
                $firstname = $user_info['firstname'];
                $lastname = $user_info['lastname'];
            }

            $_object_excel->getProperties()->setCreator($firstname . ' ' . $lastname);
            $_object_excel->getActiveSheet()->setTitle('Товари');
        }

        /**
         * Set style for header cells in report.
         *
         * @param PHPExcel $_object_excel
         * @param string   $leftCol
         * @param string   $rightCol
         * @param int      $row
         */
        private function setHeaderStyleCell(PHPExcel $_object_excel, string $leftCol, string $rightCol, int $row)
        {
            $_object_excel->getActiveSheet()->getRowDimension("{$row}")->setRowHeight(21);

            for ($character = ord($leftCol); $character <= ord($rightCol); $character++)
            {
                $_object_excel->getActiveSheet()->getColumnDimension(chr($character))->setAutoSize(true);
                $_cell = chr($character).$row;
                $_object_excel->getActiveSheet()->getStyle($_cell)->getFont()->setSize(15);
                $_object_excel->getActiveSheet()->getStyle($_cell)->getFont()->setBold(true);
                $_object_excel->getActiveSheet()->getStyle($_cell)->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_RED));

                $border_style= [
                    'borders' => [
                        'allborders' => [
                            'style' =>PHPExcel_Style_Border::BORDER_THICK,
                            'color' => [
                                'argb' => '#000000'
                            ],
                        ],
                    ],
                ];
                $_object_excel->getActiveSheet()->getStyle($_cell)->applyFromArray($border_style);
            }
            $_object_excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
            $_object_excel->getActiveSheet()->getColumnDimension('C')->setWidth(11.7);

        }
    }