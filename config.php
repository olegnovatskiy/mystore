<?php
// HTTP
    define('HTTP_SERVER', 'http://mystore/');

// HTTPS
    define('HTTPS_SERVER', 'http://mystore/');

// DIR
    define('DIR_APPLICATION', '/home/olegnovatskiy/www/mystore/catalog/');
    define('DIR_SYSTEM', '/home/olegnovatskiy/www/mystore/system/');
    define('DIR_LANGUAGE', '/home/olegnovatskiy/www/mystore/catalog/language/');
    define('DIR_TEMPLATE', '/home/olegnovatskiy/www/mystore/catalog/view/theme/');
    define('DIR_CONFIG', '/home/olegnovatskiy/www/mystore/system/config/');
    define('DIR_IMAGE', '/home/olegnovatskiy/www/mystore/image/');
    define('DIR_CACHE', '/home/olegnovatskiy/www/mystore/system/storage/cache/');
    define('DIR_DOWNLOAD', '/home/olegnovatskiy/www/mystore/system/storage/download/');
    define('DIR_LOGS', '/home/olegnovatskiy/www/mystore/system/storage/logs/');
    define('DIR_MODIFICATION', '/home/olegnovatskiy/www/mystore/system/storage/modification/');
    define('DIR_UPLOAD', '/home/olegnovatskiy/www/mystore/system/storage/upload/');

// DB
    define('DB_DRIVER', 'mysqli');
    define('DB_HOSTNAME', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '12345678');
    define('DB_DATABASE', 'mystore');
    define('DB_PORT', '3306');
    define('DB_PREFIX', 'oc_');
